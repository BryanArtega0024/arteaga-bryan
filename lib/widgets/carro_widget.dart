import 'package:flutter/material.dart';
import '../constants.dart';
import '../model/carro.dart';

class CarroWidget extends StatelessWidget {
  final Carro carro;
  final Function onTap;
  final Function onSwipe;
  final Function onLongPress;

  const CarroWidget(
      {Key? key,
      required this.carro,
      required this.onTap,
      required this.onSwipe,
      required this.onLongPress})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
        onSwipe(carro.id);
      },
      onLongPress: () {
        onLongPress(carro.id);
      },
      onTap: () {
        onTap(carro);
      },
      child: Container(
        padding: const EdgeInsets.all(10.0),
        height: 80.0,
        child: Row(
          children: [
            Expanded(
              flex: 1,
              child: Container(
                height: 12.0,
                decoration: BoxDecoration(
                    color: carro.read ? Colors.transparent : primaryColor,
                    shape: BoxShape.circle),
              ),
            ),
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text('From: ${carro.model}', style: fromTextStyle),
                  const SizedBox(height: 5.0),
                  const Divider(color: primaryColor),
                  const SizedBox(height: 5.0),
                  Text(carro.brand, style: subjectTextStyle),
                  const SizedBox(height: 5.0),
                  Text(carro.manufacturingDate.toString().substring(0, 16),
                      style: dateTextStyle),
                  const SizedBox(height: 5.0),
                  const Divider(color: primaryColor),
                  const SizedBox(height: 5.0),
                  Text(carro.description, style: bodyTextStyle),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
