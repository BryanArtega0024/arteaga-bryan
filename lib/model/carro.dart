class Carro {
  final int id;
  final String brand;
  final String model;

  final DateTime manufacturingDate;
  final String description;
  bool read;

  Carro({
    required this.id,
    required this.brand,
    required this.model,
    required this.manufacturingDate,
    required this.description,
    this.read = false,
  });
}
//////////////////////

class Calificacion {
  final int id;
  final String partial;
  final String level;
  final DateTime savedDate;
  final String course;
  bool read;

  Calificacion({
    required this.id,
    required this.partial,
    required this.level,
    required this.savedDate,
    required this.course,
    this.read = false,
  });
}

//////////////////////////77
///
class Student {
  final int id;
  final String name;
  final String last_name;
  final DateTime birth_date;
  final String observations;
  bool read;

  Student({
    required this.id,
    required this.name,
    required this.last_name,
    required this.birth_date,
    required this.observations,
    this.read = false,
  });
}

////////////////////////////////////////

class City {
  final int id;
  final String name;
  final int population;
  final DateTime foundationDate;
  bool read;

  City({
    required this.id,
    required this.name,
    required this.population,
    required this.foundationDate,
    this.read = false,
  });
}

////////////////

class Animal {
  final int id;
  final String type;
  final int age;
  final String image;
  bool read;

  Animal({
    required this.id,
    required this.type,
    required this.age,
    required this.image,
    this.read = false,
  });
}

//////////////////////////////////77

class User {
  final int id;
  final String nickname;
  final DateTime createdAt;
  final String profileImage;
  bool read;

  User({
    required this.id,
    required this.nickname,
    required this.createdAt,
    required this.profileImage,
    this.read = false,
  });
}

////////////////////////////777
class Route {
  final int id;
  final String neighborhood;
  final int frequency;
  final String image;
  bool read;

  Route({
    required this.id,
    required this.neighborhood,
    required this.frequency,
    required this.image,
    this.read = false,
  });
}
