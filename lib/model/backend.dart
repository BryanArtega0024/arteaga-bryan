import 'carro.dart';

class Backend {
  /// Singleton pattern used here.
  static final Backend _backend = Backend._internal();

  factory Backend() {
    return _backend;
  }

  Backend._internal();

  /// Private list of emails. Hardcoded here for testing purposes.
  final _carros = [
    Carro(
      id: 1,
      brand: 'r',
      model: 'RIO R',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: 'CARRO1',
    ),
    Carro(
      id: 1,
      brand: 'lamborghini',
      model: 'huracan',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: 'B',
    ),
    Carro(
      id: 2,
      brand: 'lamborghini',
      model: 'huracan',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: 'B',
    ),
    Carro(
      id: 3,
      brand: 'chebrolet',
      model: 'B',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: '',
    ),
    Carro(
      id: 4,
      brand: ' (chebrolet)',
      model: 'camaro',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: 'B',
    ),
    Carro(
      id: 5,
      brand: ' (chebrolet)',
      model: 'camaro',
      manufacturingDate: DateTime(2020, 10, 03, 19, 43),
      description: 'B',
    )
  ];
/////////////////////////////////////////////////////////////7

  final _calificaciones = [
    Calificacion(
      id: 1,
      partial: 'Primero',
      level: 'Septimo',
      savedDate: DateTime(2020, 10, 03, 19, 43),
      course: 'Fundamentos de programación',
    )
  ];

////////////////////////////////////////////////////////////////

  final _students = [
    Student(
      id: 1,
      name: 'Juan',
      last_name: 'Pueblo',
      birth_date: DateTime(2020, 10, 03, 19, 43),
      observations: 'Falta la materia de fundamentos de programación',
    )
  ];

////////////////////////////////////////////////////7777777

  final _cities = [
    City(
      id: 1,
      name: 'Manta',
      population: 400000,
      foundationDate: DateTime(2020, 10, 03, 19, 43),
    )
  ];

/////////////////////////////////////////////////////////////////////77

  final _animals = [
    Animal(
      id: 1,
      type: 'bird',
      age: 2,
      image:
          'http://www.masquesaludanimal.es//posts/asset_upload_file39075_613728.jpg',
    )
  ];
////////////////////////////////////////////////

  final _users = [
    User(
      id: 1,
      nickname: 'my_nickname',
      createdAt: DateTime(2020, 10, 03, 19, 43),
      profileImage:
          'http://www.masquesaludanimal.es//posts/asset_upload_file39075_613728.jpg',
    )
  ];

  ///////////////////////////////////////////////
  final _routes = [
    Route(
      id: 1,
      neighborhood: 'Central',
      frequency: 8,
      image: 'https://i.blogs.es/35ea69/google-maps-huawei-1/840_560.jpg',
    )
  ];

  ///
  /// Public API starts here :)
  ///

  /// Returns all emails.

//
  /// Public API starts here :)
  ///

  /// Returns all emails.
  List<Carro> getCarros() {
    return _carros;
  }

  /// Marks email identified by its id as read.
  void markCarroAsRead(int id) {
    final index = _carros.indexWhere((carro) => carro.id == id);
    _carros[index].read = true;
  }

  /// Deletes email identified by its id.
  void deleteCarro(int id) {
    _carros.removeWhere((carro) => carro.id == id);
  }
}
